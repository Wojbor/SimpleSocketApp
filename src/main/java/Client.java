import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Client {

    private String userName;
    private String password;
    private boolean status;
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;
    private int powers;

    public Client(String userName, String password, boolean status, int powers) {
        this.userName = userName;
        this.password = password;
        this.status = status;
        this.powers = powers;
    }

    public Client() {
    }


    public void startConnection(String ip, int port) throws IOException {
        clientSocket = new Socket(ip, port);
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
    }
    public String sendRequest(String request) throws IOException {
        out.println(request);
        String respond = in.readLine();
        return respond;
    }
    public String getUserData() throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("TypeLogin");
        String login = scanner.nextLine();
        out.println("getUserData");
        out.println(login);
        String response;
        response = in.readLine();
        return response;
    }

    public void stopConnection() throws IOException {
        in.close();
        out.close();
        clientSocket.close();
    }
    public boolean Log(String userName, String password) throws IOException {
        startConnection("127.0.0.1", 9494);
        out.println(userName);
        out.println(password);
        String re;
        re = in.readLine();
        System.out.println(re);
        if (re.equals("Invalid data"))
        {
            return false;
        }else
        {
            return true;
        }

    }

    public String addUser(String request) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Type login");
        String login = scanner.nextLine();
        System.out.println("Type password");
        String password = scanner.nextLine();
        out.println(request);

        out.println(login);
        out.println(password);
        String respond = in.readLine();
        return respond;
    }

    public String deleteUser(String request) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Type userName to delete him");
        String login = scanner.nextLine();
        out.println(request);
        out.println(login);
        String respond = in.readLine();
        return respond;

    }
    public String sendMsg(String request) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Type message recipient");
        String login = scanner.nextLine();
        System.out.println("Type message");
        String msg = scanner.nextLine();
        String respond;
        out.println(request);
        while (true)
        {
            if (msg.length() > 255)
            {
                System.out.println("Za dluga wiadomosc wpisz ponownie");
                msg = scanner.nextLine();
            }else
            {
                out.println(login);
                out.println(msg);
                respond = in.readLine();
                break;
            }

        }

        return respond;
    }
}
