

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;

public class Main {


    public static void main(String[] args) throws IOException {

        Scanner scanner = new Scanner(System.in);
        Client client = new Client();

        String userName;
        String password;
        boolean flag;
        System.out.println("Wypisz nazwe uzytkownika oraz haslo");
        userName = scanner.nextLine();
        password = scanner.nextLine();

        flag = client.Log(userName, password);
        String command;
        String respons;

        while (flag)
        {
            command = scanner.nextLine();

            if (command.equals("adduser"))
            {
                System.out.println(client.addUser("adduser"));

            }else if(command.equals("deleteu"))
            {
                System.out.println(client.deleteUser("deleteu"));
            }else if (command.equals("sendMsg"))
            {
                System.out.println(client.sendMsg("sendMsg"));
            }else if(command.equals("stop"))
            {
                respons = client.sendRequest(command);
                System.out.println(respons);
                break;
            }else if(command.equals("logout"))
            {
                boolean flagL = true;
                respons = client.sendRequest(command);
                System.out.println(respons);
                client.stopConnection();

                while (flagL)
                {
                    System.out.println("Type login and password");
                    userName = scanner.nextLine();
                    password = scanner.nextLine();

                    flagL = !(client.Log(userName, password));
                }

            }else if(command.equals("getUserData"))
            {
                respons = client.getUserData();
                System.out.println(respons);
            }else
            {
                respons = client.sendRequest(command);
                System.out.println(respons);
            }

        }

    }
}
