import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.*;

public class Server {

    private ServerSocket serverSocket;
    private boolean serverStatus = true;
    private Date date = new Date();
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    private Instant start = Instant.now();
    private Map<String, Object> data = new LinkedHashMap<>();
    private Map userData = new LinkedHashMap();
    private Map messages = new LinkedHashMap();
    private JSONObject jsonObject= new JSONObject();

    private final int serialNumber = randSerial();

    public void getAdmin() throws IOException {
        List<String> helpMessagesList = new LinkedList<>();
        userData.put("UserName", "admin");
        userData.put("Password", "admin");
        userData.put("Status", true);
        userData.put("Powers", 44);
        messages.put("Wiadomosci", helpMessagesList);
        File adminJson = new File("admin.json");
        if(adminJson.exists())
        {
            System.out.println("Admin exsist");
        }else
        {
            flushJsonUser(userData, messages, "admin");
        }

    }
    public void start(int port) throws IOException{

        serverSocket = new ServerSocket(port);

        getAdmin();

        while (serverStatus)
        {
            new ClientHandler(serverSocket.accept()).start();
        }
    }
    private class ClientHandler extends Thread
    {
        private Map userDataCH = new LinkedHashMap();
        private Map messagesCH = new LinkedHashMap();
        private JSONObject jsonObjectCH;
        private Socket clientSocket;
        private PrintWriter out;
        private BufferedReader in;
        private String login, password;
        private Long powers;
        private List<String> messagesList = new LinkedList<>();

        private boolean clientFlag = false;

        public ClientHandler(Socket socket)
        {
            this.clientSocket = socket;
        }
        public boolean validate(String username, String pasword)
        {
            if (userDataCH.get("UserName").equals(username) && userDataCH.get("Password").equals(pasword))
            {
                return true;
            }else
            {
                return false;
            }
        }

        public void run() {


            String input = null;
            try {
                out = new PrintWriter(clientSocket.getOutputStream(), true);
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                userDataCH.clear();
                messagesCH.clear();
                login = in.readLine();
                password = in.readLine();
                jsonObjectCH = (org.json.simple.JSONObject) getJsonData(login);
                userDataCH = (Map) jsonObjectCH.get("Użytkownik");
                messagesCH = (Map) jsonObjectCH.get("Wiadomosci");
                powers = (Long) userDataCH.get("Powers");


            } catch (IOException | ParseException e) {
                e.printStackTrace();
            }


            if (userExsist(login) && validate(login, password)) {
                System.out.println("Proper Log");
                out.println("Proper Log");
                clientFlag = true;
            } else {
                System.out.println("Invalid data");
                out.println("Invalid data");
                clientFlag = false;
            }
            while (clientFlag){
                try {
                    input = in.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if(input.equals("logout"))
                {
                    out.println("Client logout");
                    clientFlag = false;
                }

                if (input.equals("getMyData")) {

                    out.println(userDataCH.toString()  + messagesCH.toString());
                }
                if(input.equals("adduser"))
                {
                    String lo;
                    String p;

                    try {
                        lo = in.readLine();
                        p = in.readLine();
                        if (powers == 44)
                        {
                            System.out.println(lo + " "+ p);
                            flushJsonUser(lo, p);
                            out.println("User added");
                        }else
                        {
                            out.println("Permition Denaied");
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                if (input.equals("deleteu"))
                {
                    String lo;
                    try {
                        lo= in.readLine();
                        File file = new File(lo+".json");
                        if (powers == 44 && file.exists())
                        {
                            file.delete();
                            out.println("User delete");
                        }else
                        {
                            out.println("Permition Denaied or invalid user");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if (input.equals("getUserData"))
                {
                    String log = null;
                    try {
                        log = in.readLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (powers == 44)
                    {
                        System.out.println("asdasd");
                        try {
                            jsonObject = (JSONObject) getJsonData(log);
                            out.println(jsonObject.toString());
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }else
                    {
                        out.println("Permition Denaied or invalid login");
                    }
                }

                if (input.equals("sendMsg"))
                {
                    String log;
                    String msg;
                    try {
                        log = in.readLine();
                        msg = in.readLine();



                        if (userExsist(log))
                        {
                            userData.clear();
                            messages.clear();
                            JSONObject jsonObject = (JSONObject) getJsonData(log);
                            userData = (Map) jsonObject.get("Użytkownik");
                            messages = (Map) jsonObject.get("Wiadomosci");
                            messagesList = (List<String>) messages.get("Wiadomosci");

                            if(messagesList.size() < 5)
                            {
                                messagesList.add(login + ": "+ msg);
                                messages.put("Wiadomosci", messagesList);
                                System.out.println(messages.toString());
                                flushJsonUser(userData, messages, log);
                                messagesList.clear();
                                out.println("Message sent");
                            }else
                            {
                                out.println("Mailbox full");
                            }


                        }else
                        {
                            out.println("Invalid user name");
                        }


                    } catch (IOException | ParseException e) {
                        e.printStackTrace();
                    }
                }

                if ("uptime".equals(input)) {
                    Long u = upTime();
                    data.put("Uptime in sec", u);
                    String up = JSONObject.toJSONString(data);
                    data.clear();
                    System.out.println(up);
                    out.println(up);
                }

                if ("info".equals(input)) {
                    String temp;
                    data.put("Data", simpleDateFormat.format(date).toString());
                    data.put("Server_Serial_number", serialNumber);
                    temp = JSONObject.toJSONString(data);
                    data.clear();
                    out.println(temp);
                }
                if ("help".equals(input)) {
                    data.put("uptime", "zwraca czas życia serwera");
                    data.put("info", "zwraca numer wersji serwera, datę jego utworzenia");
                    data.put("help", "zwraca listę dostępnych komend z krótkim opisem");
                    data.put("stop", "zatrzymuje jednocześnie serwer i klienta");
                    data.put("sendMsg", "wyslanie wiadomosci do uzytkownika");
                    data.put("getMyData", "Wyswietlenie informacji na temat uzytkownika");
                    data.put("adduser", "dodanie uzytkownika (tylko admin)");
                    data.put("deleteu", "usuniecie uzytkownika(tylko admin)");
                    data.put("getUserData", "pobranie danych o uzytkowniku(tylko admin)");
                    String dat = JSONObject.toJSONString(data);
                    data.clear();
                    System.out.println(dat);
                    out.println(dat);

                }
                if ("stop".equals(input)) {

                    out.println("server stop");
                    serverStatus = false;
                    System.out.println(serverStatus);
                    clientFlag = false;
                    System.exit(1);
                }
            }

        }
    }


    public boolean userExsist(String login)
    {
        File file = new File(login + ".json");

        if (file.exists())
        {
            return true;
        }else
        {
            return false;
        }
    }


    public void flushJsonUser(String login, String password) throws IOException {
        List<String> msg = new LinkedList<>();
        userData.put("UserName", login);
        userData.put("Password", password);
        userData.put("Status", true);
        userData.put("Powers", 0);
        messages.put("Wiadomosci", msg);
        jsonObject.put("Użytkownik",userData);
        jsonObject.put("Wiadomosci", messages);

        FileWriter fileWriter = new FileWriter(login+".json");

        fileWriter.write(jsonObject.toJSONString());
        fileWriter.flush();
    }

    public void flushJsonUser(Map userData, Map wiadomosci, String login) throws IOException {
        jsonObject.put("Użytkownik",userData);
        jsonObject.put("Wiadomosci", wiadomosci);

        FileWriter fileWriter = new FileWriter(login+".json");

        fileWriter.write(jsonObject.toJSONString());
        fileWriter.flush();
    }

    public Object getJsonData(String userName) throws IOException, ParseException {
        Object ob = new JSONParser().parse(new FileReader((String)(userName+".json")));

        JSONObject object = (JSONObject) ob;
        return object;
    }


    public static void main(String[] args) throws IOException {

        Server server = new Server();
        server.start(9494);

    }

    public Long upTime() {
        Duration duration;
        Instant time = Instant.now();
        duration = Duration.between(start, time);
        return duration.getSeconds();
    }

    private int randSerial() {
        final int serial = (int) (Math.random() * 100000 + 1);

        return serial;
    }
}

