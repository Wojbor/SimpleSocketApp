
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.stream.Stream;

public class ClientTests {

    Client client;

    @BeforeEach
    void setUp() {
        client = new Client();
    }

    @org.junit.jupiter.api.Test
    @DisplayName("Log In")
    void logIn() throws IOException {



        Assertions.assertTrue(client.Log("arek", "1234"), String.valueOf(true));

        client.sendRequest("logout");

    }



    @Test
    @DisplayName("Get someone data without powers")
     void getSomeoneDataWithoutPowers() throws IOException {

        client.Log("arek", "1234");

        String data = "jhon";
        InputStream stdin = System.in;
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        Scanner scanner = new Scanner(System.in);


        String resposne =  client.getUserData();
        System.setIn(stdin);
        Assertions.assertEquals("Permition Denaied or invalid login", resposne);

        client.sendRequest("logout");


    }

    @Test
    @DisplayName("Admin get user data")
    void adminGetUserData() throws IOException {


        client.Log("admin", "admin");
        String s  = "{\"Wiadomosci\":{\"Wiadomosci\":[]},\"Użytkownik\":{\"Status\":true,\"UserName\":\"jhon\",\"Powers\":0,\"Password\":\"1234\"}}";

        String data = "jhon";
        InputStream stdin = System.in;
        System.setIn(new ByteArrayInputStream(data.getBytes()));


        String resposne =  client.getUserData();
        System.setIn(stdin);
        Assertions.assertEquals(s, resposne);

        client.sendRequest("logout");
    }


    @Test
    @Disabled
    @DisplayName("Create User and delete him")
    void createUserAndDeleteHim() throws IOException {


        client.Log("admin", "admin");

        String data = "test" + "test";
        InputStream stdin = System.in;
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        System.setIn(new ByteArrayInputStream(data.getBytes()));


        String resposne =  client.addUser("adduser");



        Assertions.assertEquals("User added", resposne);



        client.sendRequest("stop");
    }

    @Test
    @DisplayName("Admin delete user")
    void adminDeleteUser() throws IOException {

        client.Log("admin", "admin");

        String data = "jhon";
        InputStream stdin = System.in;
        System.setIn(new ByteArrayInputStream(data.getBytes()));

        String response =  client.deleteUser("deleteu");
        System.setIn(stdin);

        Assertions.assertEquals("User delete", response);

        client.sendRequest("logout");


    }


}
